import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);
// 导入2个一级路由
import Layout from "../views/Layout";
import Play from "../views/Play";
// 导入两个二级路由
import Home from "../views/Layout/Home";
import Search from "../views/Layout/Search";

const routes = [
  {
    path: "/",
    redirect: "/layout",
  },
  {
    path: "/layout",
    component: Layout,
    redirect: "/layout/home",
    children: [
      {
        path: "home",
        component: Home,
        meta: {
          //路由元信息，可以在这里保存更多的路由信息，恰当时机取值
          title: "首页",
        },
      },
      {
        path: "search",
        component: Search,
        meta: {
          title: "搜索",
        },
      },
    ],
  },
  {
    //播放音乐
    path: "/play/:id",
    component: Play,
  },
];

const router = new VueRouter({
  routes,
});

export default router;

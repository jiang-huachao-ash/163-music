// 导入公共的请求函数
import request from "@/utils/request";

/**
 *获取热搜列表
 */
export const getHotListAPI = () => {
    return request({
        method: "get",
        url: "/search/hot",
    });
};

/**
 *获取搜索结果列表
 *@param {*} keywords 搜索关键词
 *@param {*} limit 条数
 *@param {*} offset 分页
 */
export const getSearchResultListAPI = (keywords, limit, offset) => {
    return request({
        method: "get",
        url: "/search",
        params: {
            keywords,
            limit,
            offset,
        },
    });
};
// 1.导入公共函数request
import request from "@/utils/request";

// 2.定义具体接口函数并导出

// 获取推荐歌单列表
export const gerRecommendListAPI = (limit = 6) => {
    // 调用request函数，并返回请求结果
    return request({
        method: "get",
        url: "/personalized",
        params: {
            limit: limit,
        },
    });
};

// 获取最新音乐列表
export const gerNewSongListAPI = (limit = 10) => {
    return request({
        method: "get",
        url: "/personalized/newsong",
        params: {
            limit,
        },
    });
};
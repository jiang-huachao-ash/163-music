import axios from '@/utils/request'

// 根据id获取歌曲详情
const getSongByIdAPI = songId => {
  return axios({
    method: 'GET',
    url: '/song/detail?ids=' + songId
  })
}

// 根据id获取歌词
const getLyricByIdAPI = songId => {
  return axios({
    method: 'GET',
    url: '/lyric?id=' + songId
  })
}

export {
  getSongByIdAPI,
  getLyricByIdAPI
}

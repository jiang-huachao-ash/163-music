import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// 需求：实现vant的自动导入
// 官方推荐程序员的做法，好处可以减小打包体积
// 1. 下包
// yarn add vantalatest-v2 -S  或 npm i vantlatest-2 -S --legacy-peer-deps
// yarn add babel-plugin-import -D  或 npm i babel-plugin-import -D --legacy-peer-deps
// 2.在 babel.config.js 中 CV 官网的配置
// 3.重启服务
// 4.测试

// 需求css适配（一套样式，适配各种型号的手机）
// 方案: flexible.js + rem
// 1.下包
// yarn add amfe-flexible -S
// yarn add postcss postcss-pxtorem -D
// 2.导入
//  main.js 中 导入 amfe-flexible
import "amfe-flexible"; //行内样式无法转换

// 导入normalize.css  样式重置
import "normalize.css";
//  这个模块会把手机宽度除以10作为html元素的字体大小
// 3.src处并列创建postcss.config.js文件
// 4.重启
// 5.测试

import {
    NavBar,
    Tabbar,
    TabbarItem,
    Col,
    Row,
    Image as VanImage,
    Cell,
    CellGroup,
    Icon,
    Search,
    List,
} from "vant";
Vue.use(NavBar); //一次只能注册一个组件
Vue.use(TabbarItem);
Vue.use(Tabbar);
Vue.use(Col);
Vue.use(Row);
Vue.use(VanImage);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Icon);
Vue.use(Search);
Vue.use(List);

Vue.config.productionTip = false;
new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");
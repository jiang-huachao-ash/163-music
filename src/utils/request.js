// 对axios进行二次封装

// 1.导入axios
import axios from "axios";

// 2.创建axios实例
// 好处：如果项目有多个基地址，可以创建多个独立的axios实例，分别请求
// 得到的这个实例，与axios本身用法完全一致
const request = axios.create({
    // 基地址
    // 请求只需要写后半段地址，到达服务器的地址：基地址+请求地址
    baseURL: "http://localhost:3000",
    // 请求超时
    timeout: 500,
});

// 3.导出公共的请求函数
export default request;

// 网络请求分为三层(三个模块):
// 1.封装公共工具函数：utils/request.js -> 对 axios 做二次封装
// 2.封装接口/服务层：api/xxx.js -> 编写一个个具体向后台请求数据函数
// 3.调用接口函数：在组件中调用接口函数，发起ajax请求、完成与后台交互
# 仿网易云 demo

## 运行项目前先打开文件中的 NeteaseCloudMusicApi 文件 运行接口（后台）

```
通过cmd命令运行后台
node app.js

//通常后台接口为  http://localhost:3000/
```

## 生产依赖下载

```
npm install
```

### 运行本地服务器

```
npm run serve
```

### npm 运行项目打包

```
npm run build
```

### 项目依赖安全项

```
npm run lint
```

## 预览：

![preview1](https://gitee.com/jiang-huachao-ash/163-music/raw/master/src/assets/preview1.png)

![preview2](https://gitee.com/jiang-huachao-ash/163-music/raw/master/src/assets/preview2.png)

![preview3](https://gitee.com/jiang-huachao-ash/163-music/raw/master/src/assets/preview3.png)

## 另

1.我的 Gitee 官方博客 [https://gitee.com/jiang-huachao-ash](https://gitee.com/jiang-huachao-ash)

2.你可以通过 [https://gitee.com/jiang-huachao-ash](https://gitee.com/jiang-huachao-ash) 这个地址来了解 Ash 的优秀开源项目
